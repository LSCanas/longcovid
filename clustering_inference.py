from Clustering_models import ClustersProfiling
from Processing import aggregate_symptoms
from Prediction import PredictionClustering

import csv
import pandas as pd
import numpy as np


def inference_clustering(data_csv: str,
                         cluster_analysis: str,
                         window: list = None,
                         num_clust: int = 8,
                         variant: str = 'all',
                         list_symp: list = None,
                         list_demographic: list = None,
                         save_name: str = 'all_long'):
    """
    Produces the inference for the clustering analysis obtained by clustering_estimation.py.
    Includes the symptoms profiling based on symptoms prevalence, duration and patient's demographic per cluster and
    the prediction model based on symptoms profiling.

    :param data_csv: [str] Original data from patients' assessments (csv file name and path)
    :param cluster_analysis: [str] Cluster result obtained from clustering estimation.py
    :param window: [list[int]] List of windows of analysis in days
    :param num_clust: [int] Number of clusters. It must be smaller or equal to the max number of clusters in
    cluster_analysis
    :param variant: [str] Considers a variant of the virus based on their prevalence in the population, or uses all data
    :param list_symp: list[str] List of symptoms to be analysed. It must correspond to the symptoms considered during
    cluster estimation
    :param list_demographic: list[str] List of demographic information for the patients
    :param save_name: [str] String to be used to save the results
    :return:
    """

    if window is None:
        window = [28, 56, 84]
    if list_symp is None:
        list_symp = ['abdominal_pain', 'chest_pain', 'sore_throat', 'sob2', 'fatigue2', 'headache',
                     'hoarse_voice', 'loss_of_smell',
                     'delirium', 'diarrhoea', 'fever', 'persistent_cough', 'unusual_muscle_pains',
                     'skipped_meals']
        num_symp = len(list_symp)
    else:
        num_symp = len(list_symp)
    if list_demographic is None:
        demographic = ['patient_id', 'age', 'bmi', 'gender', 'has_diabetes', 'has_lung_disease', 'has_cancer',
                       'has_kidney_disease',
                       'has_heart_disease']
    else:
        demographic = list_demographic

    # Number of eigenvectors considered in the clustering optimisation (refers to the the dimension of the features
    # transformed)
    num_eign = 6

    data = pd.read_csv(data_csv)

    # Analysis of the symptoms according to duration
    for i, j in enumerate(window):
        print('>> Window: ', str(j), ' days')

        # Compute the symptoms aggregation for the new patients dataset
        data_, aggr, freq = aggregate_symptoms(data, window=(j, 600), frequency=True, aggregate=True)

        # Extracts the demographic information for the patients in the dataset above
        demo = data_[demographic]

        # Extracts the optimised features - features transformation matrix
        with open(variant + str(j) + '_features_cluster_' + str(num_clust) + '.csv', 'r') as f:
            reader = csv.reader(f)
            features_long = {int(rows[0]): np.array(rows[1:]).reshape(num_symp, num_eign).astype(float) for rows in
                             reader}

        # Extracts the classification (i.e., cluster label) per patient
        with open('all' + str(j) + 'class_cluster_' + str(num_clust) + '.csv', 'r') as f:
            reader = csv.reader(f)
            clusters_long = {int(rows[0]): np.array(rows[1:]) for rows in reader}

        # Clusters profiling - set of plots used to assess symptoms prevalence, duration and patient's demographic
        # per cluster
        ClustersProfiling(aggr, demo, save_name=variant+save_name+str(j)).cross_eval(cluster_analysis,
                                                                                     label_clusters='Clusters'
                                                                                     )
        # Run prediction model based on cluster analysis
        PredictionClustering(aggr, clusters_long, features_long).predictions_long(demo)

    return 0


if __name__ == '__main__':
    inference_clustering(data_csv='Data/variants20211117/All_waves.csv',
                         cluster_analysis='28_days_clusters_14.csv',
                         window=[84],
                         save_name='all_long_')
