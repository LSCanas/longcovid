import numpy as np
import pandas as pd


def read_csv(path, file_name, patients_info: str = None):
    """
    Function to read the assessments data and create the Dataframe used for further analysis
    :param path: [str] Path to the data
    :param file_name: [str] File name containing the assessments data
    :param patients_info: [str] File containing the patients data
    :return: [DataFrame] data containing all the assessments and data information
    """
    data = pd.read_csv(path + file_name, low_memory=False)
    # Removing the string noise from patients ID
    data['patient_id'] = data['patient_id'].str[2:-1]

    if patients_info is not None:
        patient_id = pd.read_csv(path + patients_info)
        data = pd.merge(data, patient_id, left_on='patient_id', right_on='id')
        data['age'] = 2021 - data['year_of_birth']

    # Transforming data time-points
    data['date_effective_test'] = pd.to_datetime(data['date_effective_test'], unit='s')

    return data


def demographic_info(data):
    """
    Extracts the basic demographic information of a population
    :param data: [DataFrame] dataframe structure containing the basic patient's demographic information including: age,
    gender, BMI, hospitalisation
    :return: prints the basic information regarding the variables information
    """
    variables = data.keys()

    # Remove multiple time-points:
    data_cleaned = data.drop_duplicates('patient_id')
    print('>> Number of Subjects: %s' % (len(data_cleaned)))

    if np.isin('gender', variables):
        gender = np.count_nonzero(data_cleaned['gender'])
        print('>> Number of Males: %s, %0.1f' % (gender, gender / len(data_cleaned) * 100))
    else:
        pass

    if np.isin('age', variables):
        age = np.mean(data_cleaned['age'])
        print('>> Age: %s, %0.2f' % (age, np.std(data_cleaned['age'].values)))
    else:
        pass

    duration = np.mean(data_cleaned['duration7'])
    print('>> Duration: %s, %0.2f' % (duration, np.std(data_cleaned['duration7'].values)))

    hospital = np.count_nonzero(data_cleaned['location'] > 1)
    print('>> Number of people hospitalised: %s, %0.1f' % (hospital, hospital / len(data_cleaned) * 100))

    return 0
