import pandas as pd
import numpy as np


def aggregate_symptoms(data: object,
                       window: tuple,
                       aggregate: bool = True,
                       frequency: bool = False,
                       symptoms: dict = None,
                       name: str = 'wild'):
    """
    Extract the duration of the symptoms based on the windows of analysis defined, performing also their aggregation in
    the corresponding window
    :param data: symptoms dataframe, obtained from assessments analysis and duration estimation for the disease
    :param window: window of analysis. Requires the starting day (i.e., day 0 from the illness) and the last day of the
    analysis. Note that requires to be define in days.
    :param aggregate: Aggregate data by week as a sum of symptoms in a given week.
    :param frequency: Computes the frequency of the symptoms.
    :param symptoms: Dictionary of symptoms to be considered in the analysis, and the operation of used for the
    aggregation.
    :param name: string to be used as name for the output.
    :return:
    """

    start, finish = window

    if symptoms is None:
        list_symptoms = {'abdominal_pain': 'sum', 'chest_pain': 'sum', 'sore_throat': 'sum', 'sob2': 'sum',
                         'fatigue2': 'sum', 'headache': 'sum', 'hoarse_voice': 'sum', 'loss_of_smell': 'sum',
                         'delirium': 'sum', 'diarrhoea': 'sum', 'fever': 'sum', 'persistent_cough': 'sum',
                         'unusual_muscle_pains': 'sum', 'skipped_meals': 'sum'}
    else:
        list_symptoms = symptoms

    data_window = data[(data['duration7'] >= start) & (data['duration7'] <= finish)]
    max_num_weeks = np.max(data_window['duration7'])
    periods = np.round(max_num_weeks/7).astype(int)
    values = np.array([])
    week = []

    # Compute the duration in weeks from the duration given in days
    for j in range(periods):
        week.append((data_window['interval_days'] >= j * 7) &
                    (data_window['interval_days'] < max_num_weeks / periods + j*7))

        values = np.append(values, j)
    data_window['week_illness'] = np.select(week, values).astype(int)

    # Compute the number maximum of weeks of the illness duration
    num_weeks = data_window.groupby(by='patient_id').agg({'week_illness': 'max'}).reset_index()
    num_weeks = num_weeks.rename(columns={'week_illness': 'num_weeks'})
    data_window_ = pd.merge(data_window, num_weeks, how='outer')

    # Aggregate symptoms per week:
    frequency_aggregate = []
    window_to_aggregate = []

    if aggregate is True:
        window_to_aggregate = data_window_.groupby(['patient_id', 'week_illness']).agg(list_symptoms).reset_index()
        window_to_aggregate.to_csv('Data/window_' + str(finish) + name + '.csv')

    if (aggregate & frequency) is True:
        window_to_frequency = data_window_.groupby(['patient_id']).agg(list_symptoms).reset_index()
        frequency_aggregate = pd.concat([window_to_frequency.iloc[:, :1], window_to_frequency.iloc[:, 1:].div(
            data_window_.groupby(['patient_id']).agg({'duration7': 'max'}).values)], axis=1)

    return data_window, window_to_aggregate, frequency_aggregate
