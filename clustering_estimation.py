from Clustering_models import LongitudinalClustering, SimpleClustering
from Processing import read_csv, aggregate_symptoms

import pandas as pd
import os
import datetime


def clustering_estimation(path_data: str,
                          file: str = None,
                          variant: str = 'All_variants',
                          analysis: str = 'longitudinal',
                          window: list = None,
                          patients_data: str = None,
                          vaccine_data: str = None,
                          date: tuple = None,
                          date_analysis: str = None):
    """
    Clustering estimation, either using longitudinal clustering, or crossectional clustering.
    :param path_data: [str] Path to original data and to store the results
    :param file: [str] file name with the data used for clustering, containing the data from patients' assessments
    (csv file name)
    :param variant: [str] Considers a variant of the virus based on their prevalence in the population, or uses all data
    :param analysis: [str] type of analysis. At this stage accepts either longitudinal clustering (Sudre et al, 2021),
    or a cross-sectional clustering, based on KMeans or affinity propagation. (default is KMeans, change to
    SimpleClustering(<parameters>).affinity_propagations(<parameters>)
    :param window: [list[int]] List of windows of analysis in days
    :param patients_data: [str] Data containing patients information (csv file name and path)
    :param vaccine_data: [str] Data containing vaccine information (csv file name and path)
    :param date: [tuple or str -> date format '%Y-%m-%d'] Period of analysis, should be defined accordingly with the
    variant in analysis
    :return: File containing features transformation and clusters classification
    """

    # Initialise variables
    if window is None:
        window = [28, 56, 84]
    if date is None:
        date = [datetime.datetime.today()]
    if date_analysis is None:
        date_analysis = str(datetime.datetime.today())

    variants = []

    # Check if the data exists
    path_data_ = path_data.split("/")
    if os.path.exists(path_data) is True:
        # Check if the data per variant has already been cleaned
        if os.path.exists(path_data_[0] + '/variants_' + date_analysis) is True:
            if len(os.listdir(path_data_[0] + '/variants')) == 0:
                ValueError("Missing data.")
            else:
                try:
                    variants = pd.read_csv(path_data_[0] + '/variants_' + date_analysis + '/' + variant + '.csv')
                except FileNotFoundError:
                    raise FileNotFoundError(path_data_[0] + '/variants_' + date_analysis + '/' + variant + '.csv')

        # Creating data per variant
        else:
            if patients_data is None:
                patients_data = 'patients_2206.csv'
            if vaccine_data is None:
                vaccine_data = 'Data/vacc_date.csv'
            if file is None:
                file = 'LongWithSymp2206.csv'
            os.mkdir(path_data_[0] + '/variants_' + date_analysis)
            data = read_csv(path_data, file, patients_info=patients_data)
            data = data.sort_values(by=['patient_id', 'date_update'])
            vaccine_info = pd.read_csv(vaccine_data).drop_duplicates(subset='patient_id', keep='first'). \
                sort_values(by=['patient_id'])
            data_vaccine_info = pd.merge(data, vaccine_info, how='outer')
            data_without_vaccine = data_vaccine_info[data_vaccine_info['date_update'] <= data_vaccine_info['date']]

            # Load the data for the variant in analysis and save it for feature analysis
            if len(date) > 1:
                variants = data_without_vaccine[(data_without_vaccine['date_effective_test'] <= date[1]) &
                                                (data_without_vaccine['date_effective_test'] >= date[0])]
            else:
                variants = data_without_vaccine[data_without_vaccine['date_effective_test'] <= date[0]]
            variants.to_csv(path_data_[0] + '/variants_' + date_analysis + '/' + variant + '.csv')

    else:
        raise ValueError('Path does not exists.')

    # Data analysis per time window
    for i, j in enumerate(window):
        print('>> Window: ', str(j), ' days')

        # Performs the symptoms aggregation
        data_, aggr_, freq_ = aggregate_symptoms(variants, window=(j, 600), frequency=True,
                                                 aggregate=True, name=variant)

        # Implements either the longitudinal or crossectional clustering
        if analysis == 'longitudinal':
            LongitudinalClustering(aggr_, num_tries=10).clustering_computation(name=variant + str(j))
        elif analysis == 'crossectional':
            SimpleClustering(aggr_).k_means(list(range(2, 15)))

    return 0


if __name__ == '__main__':
    clustering_estimation(path_data='Data/',
                          date=('20201104', '20211117'),
                          variant='All_waves',
                          analysis='longitudinal',
                          patients_data='20211117/patients_data_20211117.csv',
                          vaccine_data='20211117/vaccine_data_20211117.csv',
                          date_analysis='20211117',
                          file='20211117/data_20211117.csv')
