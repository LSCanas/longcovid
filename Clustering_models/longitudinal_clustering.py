from sklearn import metrics

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


class LongitudinalClustering(object):
    """
    This class implements the longitudinal clustering implemented by Carole H Sudre, and described at:
    >> Sudre CH, Lee KA, Lochlainn MN, Varsavsky T, Murray B, Graham MS, Menni C, Modat M, Bowyer RCE, Nguyen LH,
    Drew DA, Joshi AD, Ma W, Guo CG, Lo CH, Ganesh S, Buwe A, Pujol JC, du Cadet JL, Visconti A, Freidin MB,
    El-Sayed Moustafa JS, Falchi M, Davies R, Gomez MF, Fall T, Cardoso MJ, Wolf J, Franks PW, Chan AT, Spector TD,
    Steves CJ, Ourselin S.
    Symptom clusters in COVID-19: A potential clinical prediction tool from the COVID Symptom Study app.
    Sci Adv. 2021 Mar 19;7(12):eabd4177. doi: 10.1126/sciadv.abd4177. PMID: 33741586; PMCID: PMC7978420.
    """
    def __init__(self, data, clusters: list = None, num_tries: int = 10,
                 list_symptoms: list = None):
        """

        :param data: [DataFrame] Dataframe containing the data with all symptoms and duration.
        :param clusters: [list] list containing the number of clusters to test
        :param num_tries: [int] number of optimisations for the MCMC algorithm
        :param list_symptoms: list of symptoms considered
        """

        self.df_error_postreat = pd.DataFrame([np.arange(0, len(list(set(data[
                                                                             'patient_id'])))),
                                               list(set(data['patient_id']))]).T

        self.df_error_postreat.columns = ['ind_pid', 'patient_id']
        print(self.df_error_postreat.shape, "Error initialised")

        if clusters is None:
            clusters = list(range(2, 15))
        else:
            clusters = clusters

        if list_symptoms is None:
            self.list_symptoms = ['abdominal_pain', 'chest_pain', 'sore_throat', 'sob2', 'fatigue2', 'headache',
                                  'hoarse_voice', 'loss_of_smell',
                                  'delirium', 'diarrhoea', 'fever', 'persistent_cough', 'unusual_muscle_pains',
                                  'skipped_meals']
        else:
            self.list_symptoms = list_symptoms

        self.data = data
        self.num_tries = num_tries

        list_symptoms_agg = {'abdominal_pain': 'sum', 'chest_pain': 'sum', 'sore_throat': 'sum', 'sob2': 'sum',
                             'fatigue2': 'sum', 'headache': 'sum', 'hoarse_voice': 'sum', 'loss_of_smell': 'sum',
                             'delirium': 'sum', 'diarrhoea': 'sum', 'fever': 'sum', 'persistent_cough': 'sum',
                             'unusual_muscle_pains': 'sum', 'skipped_meals': 'sum'}

        # Create the aggregated data per subject
        self.patients_symp = data.groupby(by='patient_id').agg(list_symptoms_agg).reset_index()
        self.patients_symp = self.patients_symp.drop(['patient_id'], axis=1)
        self.clusters = clusters
        self.num_tries = num_tries

    def clustering_computation(self, name: str = 'clusters'):
        """
        :param name:
        :return:
        """
        clust = np.max(self.clusters)
        # Initialised metrics
        silhouette_scores = np.empty([len(self.clusters)])
        error_score = np.empty([len(self.clusters)])

        for i, j in enumerate(self.clusters):
            print('>>>>>>>>> Cluster ', str(j))
            c_, classif_, error_ = self.multiple_tries_cov(self.data, j,
                                                           self.num_tries, self.list_symptoms, p=6, random_seed=15)
            r = j
            t = self.num_tries
            for n in range(0, self.num_tries):
                self.df_error_postreat['error%d_numb%d' % (r, n)] = error_[n]
                self.df_error_postreat['clust%d_numb%d' % (r, n)] = 0
                for c in range(0, j):
                    self.df_error_postreat['clust%d_numb%d' % (r, n)] = np.where(
                        self.df_error_postreat['patient_id'].isin(classif_[n][c]), c,
                        self.df_error_postreat['clust%d_numb%d' % (r, n)])
            self.df_error_postreat.to_csv(name + '_C%dT%dR%d_' % (
                clust, self.num_tries, t) + '.csv')

            filter_col = [col for col in self.df_error_postreat if col.startswith('error' + str(j))]
            best_try = np.mean(self.df_error_postreat[filter_col], axis=0).idxmin(axis=0)
            try_index = np.mean(self.df_error_postreat[filter_col], axis=0).argmin()
            c_id_temp = str.split(best_try, '_')
            cluster_id = 'clust' + str(j) + '_' + c_id_temp[1]
            silhouette_scores[i] = metrics.silhouette_score(self.patients_symp, self.df_error_postreat[cluster_id],
                                                            metric='sqeuclidean')
            # BIC = n*log(RSS/n)+(k*3)*log(n);
            (np.sum(pow(self.df_error_postreat[best_try], 2), axis=0) +
             np.log(self.patients_symp.shape[0]) * self.patients_symp.shape[1] *
             np.var(self.df_error_postreat[best_try], axis=0)) / self.patients_symp.shape[0]
            error_score[i] = self.patients_symp.shape[0] * np.log(np.sum(self.df_error_postreat[best_try]) /
                                                                  self.patients_symp.shape[0]) + \
                             j * np.log(self.patients_symp.shape[0])

        # Plotting the error to decide the number of clusters
        sns.set_theme(style="whitegrid")
        fig, (ax1) = plt.subplots(1, 1, figsize=(6, 6))
        ax2 = ax1.twinx()
        ax1.plot(self.clusters, silhouette_scores, '--', color='slategrey')
        ax2.plot(self.clusters, error_score, 'o-', color="crimson")

        ax1.set_xlabel('Number of clusters')
        ax1.set_ylabel('Silhouette scores', color='slategrey')
        ax2.set_ylabel('BIC', color='crimson')
        plt.tight_layout()
        ax1.grid(linestyle='dotted', linewidth='0.5', color='grey', alpha=.4)
        plt.savefig('Longitudinal_cluster_selection_' + name + '.png', format='png', dpi=300)
        plt.show()

        # Save the cluster information from the best try
        import csv
        with open(name + '_features_cluster_%d' % clust + '.csv', 'w') as f:
            for key in c_[try_index].keys():
                f.write("%s, %s\n" % (key, c_[try_index][key].tolist()))

        with open(name + 'class_cluster_%d' % clust + '.csv', 'w') as f:
            for key in classif_[try_index].keys():
                f.write("%s, %s\n" % (key, classif_[try_index][key].tolist()))
        return c_[try_index], classif_[try_index]

    def multiple_tries_cov(self, data, num_clust, num_trials, list_features,
                           p=5, random_seed=0):
        list_c = []
        list_classif = []
        list_error = []
        df_select_ind = data.set_index(['patient_id', 'week_illness'])
        df_select_fin = df_select_ind[list_features]
        dict_cov = self.create_dict_cov(df_select_fin)
        for i in range(0, num_trials):
            print("Treating sampling ", i, " out of ", num_trials)
            c_, classif_, error_ = self.mc2pca_cov(dict_cov, df_select_fin,
                                                   num_clust=num_clust,
                                                   p=p,
                                                   num_iter=20, random_seed=i + random_seed,
                                                   drop='all',
                                                   list_pat=None)
            list_c.append(c_)
            list_classif.append(classif_)
            list_error.append(error_)
        return list_c, list_classif, list_error

    def create_dict_cov(self, df):
        dict_cov = {}
        list_patients = df.index.levels[0]
        for p in list_patients:
            dict_cov[p] = np.nan_to_num(self.cov_patient(df, p, norm=True, drop='all'))
        return dict_cov

    def cov_patient(self, df, patient, norm=True, drop='all'):
        test_init = df.loc[patient].interpolate(method='linear',
                                                limit_area='inside')
        if drop == 'any':
            test_0 = test_init.dropna(how='any')
        else:
            test_0 = test_init.fillna(0)

        test_norm = test_0 - test_0.mean()
        if norm:
            cov = np.cov(np.asarray(test_norm).T)
        else:
            cov = np.cov(np.asarray(test_0).T)
        return cov

    def mc2pca_cov(self, dict_cov, df, num_clust=5, p=2, num_iter=20, random_seed=33,
                   list_pat=None,
                   drop='all'):
        np.random.seed(random_seed)
        if random_seed is None:
            num_int = np.arange(0, len(df.index.levels[0])) % num_clust
        else:
            num_int = np.random.randint(0, high=num_clust,
                                        size=len(df.index.levels[0]), dtype='l')
        # print(num_int)
        centroid_init = []

        for f in range(0, num_clust):
            centroid_init.append([])
        list_patients = df.index.levels[0]
        if list_pat is not None:
            for (f, pat) in enumerate(list_pat):
                centroid_init[f].append(pat)
        else:
            for f in range(0, num_clust):
                centroid_init[f] = list_patients[np.where(num_int == f)]

        proj_centroids = {}
        for (i, f) in enumerate(centroid_init):
            proj_centroids[i] = self.projection_cluster_cov(dict_cov, f, p)
        patient_classif = {}
        it = 0
        old_error = 1e32
        total_error = 100000000000
        while (old_error - total_error) / old_error > 0.0001 and it < num_iter + 60:
            old_error = total_error
            error_array = np.zeros([num_clust, len(list_patients)])
            for c_ind in range(num_clust):
                proj = proj_centroids[c_ind]
                sst = np.matmul(np.asarray(proj), proj.T)
                for (p_ind, patient) in enumerate(list_patients):
                    error_array[c_ind, p_ind] = self.error_calculation(
                        sst, df.loc[patient], drop=drop)
            temp_attribution = np.argmin(error_array, 0)
            total_error = np.sum(np.min(error_array, 0))
            print("Treating iteration ", it, "Error is ", total_error, " ratio is ",
                  (old_error - total_error) / old_error)
            if total_error < old_error:
                for c_ind in range(num_clust):
                    patient_clust = list_patients[
                        np.where(temp_attribution == c_ind)]
                    proj_centroids[c_ind] = self.projection_cluster_cov(dict_cov,
                                                                        patient_clust, p)
                    patient_classif[c_ind] = patient_clust

            it += 1

        return proj_centroids, patient_classif, np.min(error_array, 0)

    def projection_cluster_cov(self, dict_cov, patient_list, numb_dim=2):
        if len(patient_list) == 0:
            print("no patient in the list")
            return None
        cov_fin = np.zeros(dict_cov[patient_list[0]].shape)

        for patient in patient_list:
            cov_temp = dict_cov[patient]
            cov_fin += cov_temp
        cov_fin /= len(patient_list)
        u, s, v = np.linalg.svd(cov_fin)
        u_array = np.asarray(u)
        return u_array[:, :numb_dim]

    def error_calculation(self, sst, array, drop='all'):
        array_new = array
        if drop == 'any':
            array_new = array_new.dropna(how='any')
        else:
            array_new = array_new.dropna(how='all')
            array_new = array_new.fillna(0)
        projected = np.matmul(np.asarray(array_new), sst)
        error = np.sum(np.square(np.asarray(array_new - projected)))
        return error
