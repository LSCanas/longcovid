from math import pi
from scipy.interpolate import interp1d

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import seaborn as sns


class ClustersProfiling:
    def __init__(self, data,
                 demographic: pd.DataFrame,
                 list_symptoms: list = None,
                 threshold_period: int = 4,
                 save_name: str = 'clustering_profiling'):

        if list_symptoms is None:
            self.list_symptoms = ['Abdominal Pain', 'Chest Pain', 'Sore Throat', 'Dysponea',
                                  'Fatigue', 'Headache',
                                  'Hoarse Voice', 'Anosmia',
                                  'Confusion', 'Diarrhoea', 'Fever',
                                  'Persistent Cough', 'Myalgias',
                                  'Low Appetite']
        else:
            self.list_symptoms = list_symptoms

        # -------------------------- Clean demographic data and prepare it for the plotting
        conditions = [
            (demographic['has_kidney_disease'] == 1),
            (demographic['has_lung_disease'] == 1),
            (demographic['has_heart_disease'] == 1),
            (demographic['has_cancer'] == 1),
            (demographic['has_diabetes'] == 1),
            ((demographic['has_kidney_disease'] != 1) &
             (demographic['has_lung_disease'] != 1) &
             (demographic['has_heart_disease'] != 1) &
             (demographic['has_cancer'] != 1) &
             (demographic['has_diabetes'] != 1))
        ]

        value = ['Kidney Disease', 'Lung Disease', 'Heart Disease', 'Cancer', 'Diabetes', 'None']
        # Create a new key to summarise all the pre-conditions
        demographic['pre_conditions'] = np.select(conditions, value).astype(str)
        demographic = demographic.drop_duplicates(['patient_id'], keep='last')

        # Clean any incorrect information
        demographic = demographic[(demographic.bmi < 90) & (demographic.bmi > 15)]
        demographic = demographic[(demographic.gender < 2) & (demographic.bmi >= 0)]
        demographic['gender'] = demographic['gender'].replace(to_replace=[0, 1], value=["Female", "Male"])

        self.demographic = demographic
        self.data = data
        self.data = data
        self.save_name = save_name
        self.threshold_period = threshold_period

    def longitudinal_evaluation(self, clusters_analysis: str,
                                cluster_num: int):

        # Loading the results of the clustering analysis
        clusters = pd.read_csv(clusters_analysis, low_memory=False)

        # Selects the best estimation based on the error of the optimisation rounds for
        # number of clusters == num_clusters
        filter_col = [col for col in clusters if col.startswith('error' + str(cluster_num))]
        error_score = np.empty([cluster_num - 1])
        for j in range(2, cluster_num + 1):
            filter_col = [col for col in clusters if col.startswith('error' + str(j))]
            best_try = np.mean(clusters[filter_col], axis=0).idxmin(axis=0)
            error_score[j - 2] = (np.sum(pow(clusters[best_try], 2), axis=0) +
                                  np.log(self.data.shape[0]) * self.data.shape[1] *
                                  np.var(clusters[best_try], axis=0)) / self.data.shape[0]
        best_try = np.mean(clusters[filter_col], axis=0).idxmin(axis=0)
        var_names = clusters.keys()
        cluster_info = var_names[np.where(clusters.columns.str.startswith('clust' + str(cluster_num)) &
                                          clusters.columns.str.endswith(best_try[6:]))[0]]

        self.clusters_evaluation(clusters, label_cluster=cluster_info[0])

    def clusters_evaluation(self, clusters: list,
                            label_cluster: str):

        # Modifies the clusters name to alphabetic rather than numeric
        clusters_id = list(map(chr, range(65, 91)))
        clusters[label_cluster] = clusters[label_cluster].replace(
            to_replace=list(range(np.max(clusters[label_cluster]) + 1)),
            value=clusters_id[:np.max(clusters[label_cluster]) + 1])

        # Estimates the median duration of the symptoms across all population (i.e., independently of the cluster) for
        # the interpolation
        subj_duration = self.data.groupby(by='patient_id').agg({'week_illness': 'max'})
        # Get the median duration of the symptoms in the population plus 2 weeks window to consider the symptoms going
        # down
        median_duration = np.median(subj_duration) + 2

        ratio_subj, symptoms_profile, num_subj, subj_duration = self.symptoms_interpolation(
            clusters=clusters[[label_cluster, 'patient_id']],
            duration=median_duration,
            threshold_period=self.threshold_period)

        # Plot symptoms profiling for ratio of subjects and average reported symptoms
        self.symptoms_profiling(ratio_subj, num_subj, threshold_period=self.threshold_period,
                                label='Ratio of subjects reporting each symptom per week',
                                figure_label='_ratio_subj_',
                                range_variables=(0, 1))

        self.symptoms_profiling(symptoms_profile, num_subj,
                                threshold_period=self.threshold_period)

        # Compute the symptoms significance per cluster according to the z-scores
        z = np.asarray(ratio_subj)
        z_mean = np.mean(z, axis=0)
        z_std = np.std(z, axis=0)

        z_average = np.asarray(symptoms_profile)
        z_mean_average = np.mean(z_average, axis=0)
        z_std_average = np.std(z_average, axis=0)

        z_ratio = []
        z_sympt = []
        for j in range(len(z)):
            z_ratio.append((z[j] - z_mean) / z_std)
            z_sympt.append((z_average[j] - z_mean_average) / z_std_average)

        # Plot the z-scores maps for ratio of subjects and average reported symptoms
        self.symptoms_profiling(z_ratio, num_subj, threshold_period=self.threshold_period,
                                label='Z-Scores of average of symptoms per patient',
                                figure_label='_zscore_ratio_subj_',
                                range_variables=(0, 1),
                                colormap="bwr"
                                )

        self.symptoms_profiling(z_sympt, num_subj, threshold_period=self.threshold_period,
                                label='Z-Scores of average of symptoms per patient',
                                figure_label='_zscore_avg_symp_',
                                range_variables=(0, 1),
                                colormap="bwr"
                                )

        # Create demographic distributions per cluster
        patients_clustered = clusters[[label_cluster, 'patient_id']]
        self.demographic_cluster_profiling(patients_clustered, self.demographic, label_cluster)

        # Link symptoms duration with the clustering analysis result
        sub_duration_ = pd.DataFrame(subj_duration, columns=[['patient_id'] + ['PID'] + ['week'] +
                                                             self.list_symptoms][0])
        sub_duration__ = pd.merge(sub_duration_,
                                  patients_clustered[[label_cluster, 'patient_id']],
                                  right_on='patient_id', left_on='patient_id',
                                  how='outer')

        sub_duration__ = sub_duration__.rename(columns={label_cluster: 'Clusters'})
        sub_duration__ = sub_duration__.iloc[:, 1:]
        self.duration_symptoms_profiling(sub_duration__)

        return 0

    def symptoms_interpolation(self, clusters: pd.DataFrame,
                               duration: float,
                               threshold_period: int = 4):

        # Variables initialisation
        un_num = np.empty([0, 1])
        subj_duration = np.empty([0, np.shape(self.data)[1] + 1])
        normalised_data = np.empty([])
        ratio_subj = []
        average_symptoms = []

        # Creates the symptoms profiling using the information of the individual symptoms labelled with a given cluster
        for i, j in enumerate(np.unique(clusters.iloc[:, 0])):
            # Retrieve all patients from a given cluster
            temp = clusters[clusters.iloc[:, 0] == j].patient_id
            data_clusters = self.data[np.isin(self.data['patient_id'], temp)]
            unique_patients = data_clusters['patient_id'].unique()

            # Retrieve the number of subjects belonging to each cluster
            un_num = np.append(un_num, len(unique_patients))

            symp_data = np.empty([0, np.round(duration).astype(int) - threshold_period])
            normalised_symp = np.empty([0, np.round(duration).astype(int) - threshold_period])
            new_freq = np.empty([])

            # Compute the interpolation or extrapolation depending on the symptoms duration
            # It computes it per symptom
            for k in data_clusters.columns[2:]:
                sub_data = np.empty([0, np.round(duration).astype(int) - threshold_period])
                for s, w in enumerate(unique_patients):
                    sub = data_clusters[data_clusters['patient_id'] == w]
                    if np.max(sub['week_illness']) >= duration:
                        f = interp1d(sub['week_illness'], sub[k])
                        new_freq = f(list(range(threshold_period, np.round(duration).astype(int))))
                    else:
                        if len(sub[k]) < threshold_period:
                            new_freq[:len(sub[k])] = np.nan
                        else:
                            new_freq = np.zeros([np.round(duration).astype(int) - threshold_period, ])
                            new_freq[:len(sub[k][threshold_period:])] = sub[k][threshold_period:]
                        # if len(sub) == 1:
                        #     new_freq = sub[k]
                        # else:
                        #     f = np.poly1d(np.polyfit(sub['week_illness'], sub[k], 1))
                        #     new_freq = f(list(range(threshold_period, np.round(duration).astype(int))))
                        #     new_freq[new_freq < 0] = 0
                    sub_data = np.append(sub_data, new_freq[None, :], axis=0)

                    sub_data[sub_data <= 1] = 0
                    mask = np.any(np.isnan(sub_data), axis=1)
                    normalised_data = sub_data[~mask]

                normalised_symp = np.append(normalised_symp,
                                            np.count_nonzero(normalised_data, axis=0)[None, :] / len(normalised_data),
                                            axis=0)
                symp_data = np.append(symp_data, np.nanmean(sub_data, axis=0)[None, :], axis=0)

            subj_duration = np.append(subj_duration,
                                      data_clusters.groupby(by='patient_id').apply(lambda column: (column != 0).sum()).
                                      rename(columns={'patient_id': 'PID'}).reset_index(), axis=0)

            ratio_subj.append(normalised_symp.T)
            average_symptoms.append(symp_data.T)

        return ratio_subj, average_symptoms, un_num, subj_duration

    def symptoms_profiling(self, data: list,
                           num_subj: list,
                           threshold_period: int = 4,
                           label: str = 'Average symptoms reported in each week',
                           figure_label: str = '_symp_average_',
                           colormap: str = 'rocket_r',
                           range_variables: tuple = None):

        if range_variables is None:
            v_min, v_max = (0, 5)
        else:
            v_min, v_max = range_variables

        for i in range(len(data)):
            _, ax = plt.subplots(1, 1)
            sns.set_theme(style="ticks", palette="colorblind")
            data_cluster = pd.DataFrame(data[i])
            data_cluster.columns = self.list_symptoms
            data_cluster.index = data_cluster.index + threshold_period

            ax = sns.heatmap(data=data_cluster.T, cmap=colormap, vmax=v_max, vmin=v_min,
                             cbar_kws={'label': label})
            ax.set_xlabel('Week of Disease')
            ax.set_title('Cluster %s, N: %d' % (i, num_subj[i]))
            plt.tight_layout()
            plt.savefig(self.save_name + figure_label + str(i) + '.pdf', format='pdf',
                        dpi=300)
        return 0

    def demographic_cluster_profiling(self, data_patients, data_demographic, label):

        data_demographic.gender.rename('Gender')
        patients_clustered = pd.merge(data_patients, data_demographic, how='outer')
        patients_clustered = patients_clustered.dropna(subset=[label])

        # Sort the subjects according to the clusters label
        patients_clustered = patients_clustered.sort_values(by=label)

        # Plot Age distributions, conditioned by gender
        _, ax = plt.subplots(1, 1)
        sns.set_theme(style="ticks", palette="colorblind")
        ax = sns.violinplot(x=label, y="age",
                            data=patients_clustered, hue='gender', trim=True)
        ax.set_xlabel('Cluster')
        ax.set_ylabel('Age')
        sns.despine(offset=10.5)
        plt.legend(bbox_to_anchor=(1.2, 0.45), loc='center right', title='Gender')
        plt.tight_layout()
        plt.savefig(self.save_name + '_age_' + '.pdf', format='pdf', dpi=300)

        # Plot BMI distributions, conditioned by gender
        _, ax1 = plt.subplots(1, 1)
        ax1 = sns.violinplot(x=label, y="bmi",
                             data=patients_clustered, hue='gender', trim=True)
        ax1.set_xlabel('Cluster')
        ax1.set_ylabel('Body Mass Index')
        plt.legend(bbox_to_anchor=(1.2, 0.45), loc='center right', title='Gender')
        plt.tight_layout()
        sns.despine(offset=10.5)
        plt.savefig(self.save_name + '_bmi_.pdf', format='pdf', dpi=300)

        # Plot the pre-conditions distribution
        _, ax2 = plt.subplots(1, 1)
        patients_clustered = patients_clustered.sort_values(by='pre_conditions', ascending=False)
        ratio_pre_cond = patients_clustered.groupby(by=[label, "pre_conditions"]).apply(
            lambda x: x.pre_conditions.count()).unstack().T / patients_clustered.groupby(by=[label]).size()
        ratio = ratio_pre_cond.T.reset_index().melt(id_vars=label)
        ratio = ratio.replace(np.nan, 0)
        sns.set_theme(style="darkgrid", palette="colorblind")
        ax2 = sns.catplot(y="value", x='pre_conditions', col_wrap=2,
                          data=ratio, kind='bar', col=label)
        (ax2.set_axis_labels("Percentage", "Comorbidity")
         .set_titles("Cluster {col_name}")
         .despine(left=True))
        ax2.set_xticklabels(rotation=45)
        plt.tight_layout()
        plt.savefig(self.save_name + '_pre_conditions.pdf', format='pdf',
                    dpi=300)

        return 0

    def duration_symptoms_profiling(self, data_duration):

        # Defining the radar plot
        sns.set_theme(style="darkgrid", palette="colorblind")

        for idx, clusters in enumerate(data_duration.Clusters.unique()):
            ax = plt.subplot(111, polar=True)
            N = data_duration.shape[1] - 3
            angles = [n / float(N) * 2 * pi for n in range(N)]
            angles += angles[:1]

            ax.set_theta_offset(pi / 2)
            ax.set_theta_direction(-1)

            # Draw one axe per variable + add labels
            plt.xticks(angles[:-1], data_duration.columns[2:-1])

            # Draw ylabels

            ax.set_rlabel_position(90)
            plt.yticks([0, 2, 4, 6, 8, 10, 12, 14], ["4", "6", "8", "10", "12", "14", "16", "18"], color="grey",
                       size=18)
            plt.ylim(0, np.max(np.median(data_duration.iloc[:, 2:-1], axis=0)) + 8)

            COLORS = ["#FF5A5F", "#FFB400", "#007A87", "#3d85c6", "#7f6000", "#a64d79", "#6a329f", "#38761d", "#b6d7a8"]
            # Plot lines and dots
            sns.set(font_scale=0.75)

            duration = data_duration[data_duration.Clusters == clusters].median()
            values = duration.drop(["PID", "week_illness"]).values.tolist()
            values += values[:1]
            ax.plot(angles, values, c=COLORS[idx], linewidth=2.5, label=clusters)

            # Save the graph
            plt.legend(loc='lower center', title='Cluster', bbox_to_anchor=(0.2, -.3, 0.5, 0.5), facecolor='w',
                       edgecolor='w',
                       ncol=3)
            ax.set_xticklabels(data_duration.columns[2:-1], size=20, linespacing=20.0)
            plt.tight_layout()
            plt.savefig(self.save_name + '_' + clusters + '.pdf', format='pdf', dpi=300)

        return 0

    def cross_eval(self, clusters_analysis: str,
                   label_clusters: str):

        clusters = pd.read_csv(clusters_analysis)

        self.clusters_evaluation(clusters, label_clusters)

        return 0
