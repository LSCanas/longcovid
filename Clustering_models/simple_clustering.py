from sklearn.cluster import KMeans, AffinityPropagation
from sklearn import metrics

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


class SimpleClustering(object):
    def __init__(self, data, list_symptoms: dict = None, name: str = '28_days'):
        # List of symptoms in analysis
        if list_symptoms is None:
            list_symptoms = {'abdominal_pain': 'sum', 'chest_pain': 'sum', 'sore_throat': 'sum', 'sob2': 'sum',
                             'fatigue2': 'sum', 'headache': 'sum', 'hoarse_voice': 'sum', 'loss_of_smell': 'sum',
                             'delirium': 'sum', 'diarrhoea': 'sum', 'fever': 'sum', 'persistent_cough': 'sum',
                             'unusual_muscle_pains': 'sum', 'skipped_meals': 'sum'}

        # Create the aggregated data per subject
        self.patients_symp = data.groupby(by='patient_id').agg(list_symptoms).reset_index()
        self.patient_id = data.patient_id.to_numpy()
        self.save_name = name

    def k_means(self, num_clusters: list = None):
        # Drop strings with patients ID
        if num_clusters is None:
            num_clusters = [6]

        self.patients_symp = self.patients_symp.drop(['patient_id'], axis=1)
        unique_patients = self.patients_symp.shape[0]

        # Initialised metrics
        silhouette_scores = np.empty([len(num_clusters)])
        inercia_score = np.empty([len(num_clusters)])

        for i, j in enumerate(num_clusters):
            if num_clusters[i] >= unique_patients:
                j = unique_patients

            k_means = KMeans(n_clusters=j, random_state=0)
            clusters = k_means.fit_predict(self.patients_symp)
            silhouette_scores[i] = metrics.silhouette_score(self.patients_symp, clusters, metric='sqeuclidean')
            inercia_score[i] = k_means.inertia_ / unique_patients

        self.error(num_clusters, silhouette_scores, inercia_score)

        features = self.patients_symp.columns[np.argsort(k_means.cluster_centers_, axis=1)[:, -4:]]

        clusters_result = pd.DataFrame(np.concatenate([clusters[:, None], self.patient_id[:, None]], axis=1))
        clusters_result.columns = ['Clusters', 'patient_id']

        clusters_result.to_csv(self.save_name+'_clusters_%d.csv' % np.max(num_clusters))
        pd.DataFrame(features).to_csv(self.save_name+'_features_%d.csv' % np.max(num_clusters))

        return clusters_result, features

    def affinity_propagation(self, num_clusters: list = None):
        self.patients_symp = self.patients_symp.drop(['patient_id'], axis=1)
        unique_patients = self.patients_symp.shape[0]

        # Initialised metrics
        silhouette_scores = np.empty([len(num_clusters)])
        inercia_score = np.empty([len(num_clusters)])

        for i, j in enumerate(num_clusters):
            if num_clusters[i] >= unique_patients:
                j = unique_patients
                raise ValueError('Number of clusters %d is bigger than number of subjects' % (num_clusters[i], j))

            affinity = AffinityPropagation(random_state=0)
            clusters = affinity.fit_predict(self.patients_symp)
            silhouette_scores[i] = metrics.silhouette_score(self.patients_symp, clusters, metric='sqeuclidean')

        self.error(num_clusters, silhouette_scores, inercia_score)
        features = self.patients_symp.columns[np.argsort(affinity.cluster_centers_, axis=1)[:, -4:]]

        clusters_result = pd.DataFrame(np.concatenate([clusters[:, None], self.patient_id[:, None]], axis=1))
        clusters_result.columns = ['Clusters', 'patient_id']
        clusters_result.to_csv(self.save_name+'_clusters_%d.csv' % np.max(num_clusters))
        pd.DataFrame(features).to_csv(self.save_name+'_features_%d.csv' % np.max(num_clusters))

        return clusters_result, features

    def error(self, num_clusters, silhouette_scores, error_scores):

        # Plotting the error to decide the number of clusters
        sns.set_theme(style="whitegrid")
        fig, (ax1) = plt.subplots(1, 1, figsize=(6, 6))
        ax2 = ax1.twinx()
        ax1.plot(num_clusters, silhouette_scores, '--', color='slategrey')
        ax2.plot(num_clusters, error_scores, 'o-', color="crimson")

        ax1.set_xlabel('Number of clusters')
        ax1.set_ylabel('Silhouette scores', color='slategrey')
        ax2.set_ylabel('Inercia score', color='crimson')
        plt.tight_layout()
        ax1.grid(linestyle='dotted', linewidth='0.5', color='grey', alpha=.4)
        plt.savefig('K_means_cluster_selection_' + self.save_name + '.png', format='png', dpi=300)
        plt.show()

        return 0
